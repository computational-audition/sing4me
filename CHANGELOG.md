# Change Log
All important changes to the repp package will be documented here.

The changelog format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) and the project uses [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.0.0]
- Removed all psynet related dependencies
- Included only singing extract and demos
- Made it public

## [v1.3.0]
### Added
- Improved extraction by relaxing failing criteria and adding failing reason in plots

## [v1.2.0]
### Added
- Praat functionality to extract syllables/ rhythm

## [v1.1.3]
### Added
- Added params for rhythm experimetns with singing

## [v1.1.3]
### Change
- Changed package name to sing4me

## [v1.1.1]
### Added
- Added new package name: sing4me


## [v1.1.0]
### Added
- Created SING package
